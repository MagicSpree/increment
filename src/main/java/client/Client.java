package client;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Client {

    private static final Logger log = Logger.getLogger(Client.class);

    private Socket clientSocket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private boolean isConnected = false;


    Client(String ip, int port) {
        startConnection(ip, port);
    }

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            outputStream = clientSocket.getOutputStream();
            inputStream = clientSocket.getInputStream();
            log.info("Установлено соединение с сервером");
            isConnected = true;
        } catch (IOException e) {
            log.error(e);
        }
    }

    public void startConversation() throws IOException {
        int response = 1;
        sendMessage(response);
        log.info("Отправлено сообщение: " + response);

        while ((response = readMessageFromServer()) != -1) {
            log.info("Получено сообщение от сервера :" + response);
            if (response >= 10) {
                stopConnection();
                break;
            }
            sendMessage(++response);
            log.info("Отправлено сообщение: " + response);
        }
    }


    private void sendMessage(int number) throws IOException {
        outputStream.write(number);
        outputStream.flush();
    }

    private int readMessageFromServer() throws IOException {
        return inputStream.read();
    }

    public void stopConnection() throws IOException {
        inputStream.close();
        outputStream.close();
        clientSocket.close();
    }

    public boolean isConnected() {
        return isConnected;
    }
}
