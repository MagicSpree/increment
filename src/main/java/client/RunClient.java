package client;

import org.apache.log4j.BasicConfigurator;

import java.io.IOException;

public class RunClient {

    private final static String IP_ADDR = "127.0.0.1";
    private final static int PORT = 8000;

    public static void main(String[] args) {
        BasicConfigurator.configure();
        Client client = new Client(IP_ADDR, PORT);
        try {
            if (client.isConnected()) {
                client.startConversation();
            }
            System.out.println("Ошибка подключения к серверу.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

