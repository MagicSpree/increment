package server;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class IncrementServer {

    private static final Logger log = Logger.getLogger(IncrementServer.class);

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private InputStream inputStream;
    private OutputStream outputStream;

    public void start(int port) {
        try {
            serverSocket = new ServerSocket(port);
            log.info("Сервер запущен...");
            initBuffer();
            log.info("Новое соединение: " + clientSocket.getInetAddress().toString());
            int request;
            while ((request = readMessageFromServer()) != -1) {
                log.info("Прислано клиентом: " + request);
                sendMessage(request);
            }
            log.info("Клиент отключился");
        } catch (IOException e) {
            log.error(e);
        } finally {
            stop();
        }
    }

    public void initBuffer() throws IOException {
        clientSocket = serverSocket.accept();
        inputStream = clientSocket.getInputStream();
        outputStream = clientSocket.getOutputStream();
    }

    public void stop() {
        try {
            serverSocket.close();
            inputStream.close();
            outputStream.close();
            clientSocket.close();
        } catch (IOException e) {
            log.error(e);
        }

    }

    private void sendMessage(int request) throws IOException {
        outputStream.write(++request);
        log.info("Отправлено клиенту: " + request);
    }

    private int readMessageFromServer() throws IOException {
        int request = inputStream.read();
        return request;
    }
}
