package server;

import org.apache.log4j.BasicConfigurator;

public class RunServer {
    public static void main(String[] args) {
        BasicConfigurator.configure();
        IncrementServer server = new IncrementServer();
        server.start(8000);
    }
}
